# StrawberryFields FastAPI

- [3.0.0]
- [2.9.0] - 2024-08-10 - 9 files changed, 170 insertions(+), 95 deletions(-)
- [2.8.0] - 2024-03-10 - 2 files changed, 111 insertions(+), 41 deletions(-)
- [2.7.0] - 2024-02-04 - 6 files changed, 78 insertions(+), 35 deletions(-)
- [2.6.0] - 2023-11-11 - 9 files changed, 63 insertions(+), 51 deletions(-)
- [2.5.0] - 2023-11-05 - 11 files changed, 172 insertions(+), 85 deletions(-)
- [2.4.0] - 2023-04-16 - 6 files changed, 102 insertions(+), 48 deletions(-)
- [2.3.0] - 2022-11-27 - 9 files changed, 50 insertions(+), 26 deletions(-)
- [2.2.0] - 2022-11-20 - 4 files changed, 9 insertions(+), 7 deletions(-)
- [2.1.0] - 2022-06-26 - 4 files changed, 19 insertions(+), 30 deletions(-)
- [2.0.0] - 2022-03-13 - 4 files changed, 259 insertions(+), 59 deletions(-)
- [1.6.0] - 2021-10-11 - 1 files changed, 46 insertions(+), 2 deletions(-)
- [1.5.0] - 2021-10-01 - 3 files changed, 9 insertions(+), 2 deletions(-)
- [1.4.0] - 2021-09-25 - 5 files changed, 17 insertions(+), 12 deletions(-)
- [1.3.0] - 2021-04-26 - 3 files changed, 19 insertions(+), 8 deletions(-)
- [1.2.0] - 2021-02-24 - 5 files changed, 128 insertions(+), 126 deletions(-)
- [1.1.0] - 2021-01-26 - 1 files changed, 18 insertions(+), 18 deletions(-)
- 1.0.0 - 2021-01-10

[3.0.0]: https://gitlab.com/acefed/strawberryfields-fastapi/-/compare/6bdbc9b2...master
[2.9.0]: https://gitlab.com/acefed/strawberryfields-fastapi/-/compare/fda230b3...6bdbc9b2
[2.8.0]: https://gitlab.com/acefed/strawberryfields-fastapi/-/compare/3baa53ce...fda230b3
[2.7.0]: https://gitlab.com/acefed/strawberryfields-fastapi/-/compare/257fd042...3baa53ce
[2.6.0]: https://gitlab.com/acefed/strawberryfields-fastapi/-/compare/3becb579...257fd042
[2.5.0]: https://gitlab.com/acefed/strawberryfields-fastapi/-/compare/64084609...3becb579
[2.4.0]: https://gitlab.com/acefed/strawberryfields-fastapi/-/compare/e5671221...64084609
[2.3.0]: https://gitlab.com/acefed/strawberryfields-fastapi/-/compare/80ddc6fb...e5671221
[2.2.0]: https://gitlab.com/acefed/strawberryfields-fastapi/-/compare/fe2aab86...80ddc6fb
[2.1.0]: https://gitlab.com/acefed/strawberryfields-fastapi/-/compare/1a0e295a...fe2aab86
[2.0.0]: https://gitlab.com/acefed/strawberryfields-fastapi/-/compare/7a1c85a9...1a0e295a
[1.6.0]: https://gitlab.com/acefed/strawberryfields-fastapi/-/compare/5901c997...7a1c85a9
[1.5.0]: https://gitlab.com/acefed/strawberryfields-fastapi/-/compare/1f777240...5901c997
[1.4.0]: https://gitlab.com/acefed/strawberryfields-fastapi/-/compare/caef98c3...1f777240
[1.3.0]: https://gitlab.com/acefed/strawberryfields-fastapi/-/compare/27df11d2...caef98c3
[1.2.0]: https://gitlab.com/acefed/strawberryfields-fastapi/-/compare/845129dc...27df11d2
[1.1.0]: https://gitlab.com/acefed/strawberryfields-fastapi/-/compare/a7ab7df6...845129dc
