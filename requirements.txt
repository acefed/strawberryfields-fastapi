fastapi==0.115.2
uvicorn==0.32.0
aiofiles==24.1.0
cryptography==43.0.3
httpx==0.27.2
python-dotenv==1.0.1
