import base64
from datetime import datetime, timezone
import hashlib
import json
import os
import time
from urllib.parse import urlparse
from fastapi import FastAPI, HTTPException
from fastapi.requests import Request
from fastapi.responses import Response, PlainTextResponse, JSONResponse, RedirectResponse
from fastapi.staticfiles import StaticFiles
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.asymmetric import padding
import httpx
from dotenv import load_dotenv

load_dotenv()
if os.getenv("CONFIG_JSON"):
    config_json = os.getenv("CONFIG_JSON")
    if config_json.startswith("'"):
        config_json = config_json[1:]
    if config_json.endswith("'"):
        config_json = config_json[:-1]
    CONFIG = json.loads(config_json)
else:
    with open("data/config.json", "r") as f:
        CONFIG = json.load(f)
ME = "".join(
    [
        '<a href="https://',
        urlparse(CONFIG["actor"][0]["me"]).hostname,
        '/" rel="me nofollow noopener noreferrer" target="_blank">',
        "https://",
        urlparse(CONFIG["actor"][0]["me"]).hostname,
        "/",
        "</a>",
    ]
)
private_key_pem = os.getenv("PRIVATE_KEY")
private_key_pem = "\n".join(private_key_pem.split("\\n"))
if private_key_pem.startswith('"'):
    private_key_pem = private_key_pem[1:]
if private_key_pem.endswith('"'):
    private_key_pem = private_key_pem[:-1]
PRIVATE_KEY = serialization.load_pem_private_key(
    private_key_pem.encode("ascii"),
    password=None,
)
public_key_pem = (
    PRIVATE_KEY.public_key()
    .public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo,
    )
    .decode("ascii")
)

app = FastAPI(openapi_url=None, docs_url=None, redoc_url=None, swagger_ui_oauth2_redirect_url=None)


def uuidv7():
    v = bytearray(os.urandom(16))
    ts = int(time.time() * 1000)
    v[0] = ts >> 40 & 0xFF
    v[1] = ts >> 32 & 0xFF
    v[2] = ts >> 24 & 0xFF
    v[3] = ts >> 16 & 0xFF
    v[4] = ts >> 8 & 0xFF
    v[5] = ts & 0xFF
    v[6] = v[6] & 0x0F | 0x70
    v[8] = v[8] & 0x3F | 0x80
    return "".join(f"{b:02x}" for b in v)


def talk_script(req):
    ts = int(time.time() * 1000)
    if urlparse(req).hostname == "localhost":
        return f"<p>{ts}</p>"
    return "".join(
        [
            "<p>",
            '<a href="https://',
            urlparse(req).hostname,
            '/" rel="nofollow noopener noreferrer" target="_blank">',
            urlparse(req).hostname,
            "</a>",
            "</p>",
        ]
    )


async def get_activity(username, hostname, req):
    t = datetime.now(timezone.utc).strftime("%a, %d %b %Y %H:%M:%S GMT")
    sig = PRIVATE_KEY.sign(
        "\n".join(
            [
                f"(request-target): get {urlparse(req).path}",
                f"host: {urlparse(req).hostname}",
                f"date: {t}",
            ]
        ).encode("ascii"),
        padding.PKCS1v15(),
        hashes.SHA256(),
    )
    b64 = base64.b64encode(sig).decode()
    headers = {
        "Date": t,
        "Signature": ",".join(
            [
                f'keyId="https://{hostname}/u/{username}#Key"',
                'algorithm="rsa-sha256"',
                'headers="(request-target) host date"',
                f'signature="{b64}"',
            ]
        ),
        "Accept": "application/activity+json",
        "Accept-Encoding": "identity",
        "Cache-Control": "no-cache",
        "User-Agent": f"StrawberryFields-FastAPI/3.0.0 (+https://{hostname}/)",
    }
    async with httpx.AsyncClient() as client:
        res = await client.get(req, headers=headers)
        status = res.status_code
        print(f"GET {req} {status}")
        return res.json()


async def post_activity(username, hostname, req, x):
    t = datetime.now(timezone.utc).strftime("%a, %d %b %Y %H:%M:%S GMT")
    body = json.dumps(x, ensure_ascii=False, separators=(",", ":"))
    s256 = base64.b64encode(hashlib.sha256(body.encode()).digest()).decode()
    sig = PRIVATE_KEY.sign(
        "\n".join(
            [
                f"(request-target): post {urlparse(req).path}",
                f"host: {urlparse(req).hostname}",
                f"date: {t}",
                f"digest: SHA-256={s256}",
            ]
        ).encode("ascii"),
        padding.PKCS1v15(),
        hashes.SHA256(),
    )
    b64 = base64.b64encode(sig).decode()
    headers = {
        "Date": t,
        "Digest": f"SHA-256={s256}",
        "Signature": ",".join(
            [
                f'keyId="https://{hostname}/u/{username}#Key"',
                'algorithm="rsa-sha256"',
                'headers="(request-target) host date digest"',
                f'signature="{b64}"',
            ]
        ),
        "Accept": "application/json",
        "Accept-Encoding": "gzip",
        "Cache-Control": "max-age=0",
        "Content-Type": "application/activity+json",
        "User-Agent": f"StrawberryFields-FastAPI/3.0.0 (+https://{hostname}/)",
    }
    print(f"POST {req} {body}")
    async with httpx.AsyncClient() as client:
        await client.post(req, data=body.encode(), headers=headers)


async def accept_follow(username, hostname, x, y):
    aid = uuidv7()
    body = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": f"https://{hostname}/u/{username}/s/{aid}",
        "type": "Accept",
        "actor": f"https://{hostname}/u/{username}",
        "object": y,
    }
    await post_activity(username, hostname, x["inbox"], body)


async def follow(username, hostname, x):
    aid = uuidv7()
    body = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": f"https://{hostname}/u/{username}/s/{aid}",
        "type": "Follow",
        "actor": f"https://{hostname}/u/{username}",
        "object": x["id"],
    }
    await post_activity(username, hostname, x["inbox"], body)


async def undo_follow(username, hostname, x):
    aid = uuidv7()
    body = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": f"https://{hostname}/u/{username}/s/{aid}#Undo",
        "type": "Undo",
        "actor": f"https://{hostname}/u/{username}",
        "object": {
            "id": f"https://{hostname}/u/{username}/s/{aid}",
            "type": "Follow",
            "actor": f"https://{hostname}/u/{username}",
            "object": x["id"],
        },
    }
    await post_activity(username, hostname, x["inbox"], body)


async def like(username, hostname, x, y):
    aid = uuidv7()
    body = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": f"https://{hostname}/u/{username}/s/{aid}",
        "type": "Like",
        "actor": f"https://{hostname}/u/{username}",
        "object": x["id"],
    }
    await post_activity(username, hostname, y["inbox"], body)


async def undo_like(username, hostname, x, y):
    aid = uuidv7()
    body = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": f"https://{hostname}/u/{username}/s/{aid}#Undo",
        "type": "Undo",
        "actor": f"https://{hostname}/u/{username}",
        "object": {
            "id": f"https://{hostname}/u/{username}/s/{aid}",
            "type": "Like",
            "actor": f"https://{hostname}/u/{username}",
            "object": x["id"],
        },
    }
    await post_activity(username, hostname, y["inbox"], body)


async def announce(username, hostname, x, y):
    aid = uuidv7()
    t = datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%SZ")
    body = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": f"https://{hostname}/u/{username}/s/{aid}/activity",
        "type": "Announce",
        "actor": f"https://{hostname}/u/{username}",
        "published": t,
        "to": ["https://www.w3.org/ns/activitystreams#Public"],
        "cc": [f"https://{hostname}/u/{username}/followers"],
        "object": x["id"],
    }
    await post_activity(username, hostname, y["inbox"], body)


async def undo_announce(username, hostname, x, y, z):
    aid = uuidv7()
    body = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": f"https://{hostname}/u/{username}/s/{aid}#Undo",
        "type": "Undo",
        "actor": f"https://{hostname}/u/{username}",
        "object": {
            "id": f"{z}/activity",
            "type": "Announce",
            "actor": f"https://{hostname}/u/{username}",
            "to": ["https://www.w3.org/ns/activitystreams#Public"],
            "cc": [f"https://{hostname}/u/{username}/followers"],
            "object": x["id"],
        },
    }
    await post_activity(username, hostname, y["inbox"], body)


async def create_note(username, hostname, x, y):
    aid = uuidv7()
    t = datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%SZ")
    body = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": f"https://{hostname}/u/{username}/s/{aid}/activity",
        "type": "Create",
        "actor": f"https://{hostname}/u/{username}",
        "published": t,
        "to": ["https://www.w3.org/ns/activitystreams#Public"],
        "cc": [f"https://{hostname}/u/{username}/followers"],
        "object": {
            "id": f"https://{hostname}/u/{username}/s/{aid}",
            "type": "Note",
            "attributedTo": f"https://{hostname}/u/{username}",
            "content": talk_script(y),
            "url": f"https://{hostname}/u/{username}/s/{aid}",
            "published": t,
            "to": ["https://www.w3.org/ns/activitystreams#Public"],
            "cc": [f"https://{hostname}/u/{username}/followers"],
        },
    }
    await post_activity(username, hostname, x["inbox"], body)


async def create_note_image(username, hostname, x, y, z):
    aid = uuidv7()
    t = datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%SZ")
    body = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": f"https://{hostname}/u/{username}/s/{aid}/activity",
        "type": "Create",
        "actor": f"https://{hostname}/u/{username}",
        "published": t,
        "to": ["https://www.w3.org/ns/activitystreams#Public"],
        "cc": [f"https://{hostname}/u/{username}/followers"],
        "object": {
            "id": f"https://{hostname}/u/{username}/s/{aid}",
            "type": "Note",
            "attributedTo": f"https://{hostname}/u/{username}",
            "content": talk_script("https://localhost"),
            "url": f"https://{hostname}/u/{username}/s/{aid}",
            "published": t,
            "to": ["https://www.w3.org/ns/activitystreams#Public"],
            "cc": [f"https://{hostname}/u/{username}/followers"],
            "attachment": [
                {
                    "type": "Image",
                    "mediaType": z,
                    "url": y,
                },
            ],
        },
    }
    await post_activity(username, hostname, x["inbox"], body)


async def create_note_mention(username, hostname, x, y, z):
    aid = uuidv7()
    t = datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%SZ")
    at = f"@{y['preferredUsername']}@{urlparse(y['inbox']).hostname}"
    body = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": f"https://{hostname}/u/{username}/s/{aid}/activity",
        "type": "Create",
        "actor": f"https://{hostname}/u/{username}",
        "published": t,
        "to": ["https://www.w3.org/ns/activitystreams#Public"],
        "cc": [f"https://{hostname}/u/{username}/followers"],
        "object": {
            "id": f"https://{hostname}/u/{username}/s/{aid}",
            "type": "Note",
            "attributedTo": f"https://{hostname}/u/{username}",
            "inReplyTo": x["id"],
            "content": talk_script(z),
            "url": f"https://{hostname}/u/{username}/s/{aid}",
            "published": t,
            "to": ["https://www.w3.org/ns/activitystreams#Public"],
            "cc": [f"https://{hostname}/u/{username}/followers"],
            "tag": [
                {
                    "type": "Mention",
                    "name": at,
                },
            ],
        },
    }
    await post_activity(username, hostname, y["inbox"], body)


async def create_note_hashtag(username, hostname, x, y, z):
    aid = uuidv7()
    t = datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%SZ")
    body = {
        "@context": [
            "https://www.w3.org/ns/activitystreams",
            {"Hashtag": "as:Hashtag"},
        ],
        "id": f"https://{hostname}/u/{username}/s/{aid}/activity",
        "type": "Create",
        "actor": f"https://{hostname}/u/{username}",
        "published": t,
        "to": ["https://www.w3.org/ns/activitystreams#Public"],
        "cc": [f"https://{hostname}/u/{username}/followers"],
        "object": {
            "id": f"https://{hostname}/u/{username}/s/{aid}",
            "type": "Note",
            "attributedTo": f"https://{hostname}/u/{username}",
            "content": talk_script(y),
            "url": f"https://{hostname}/u/{username}/s/{aid}",
            "published": t,
            "to": ["https://www.w3.org/ns/activitystreams#Public"],
            "cc": [f"https://{hostname}/u/{username}/followers"],
            "tag": [
                {
                    "type": "Hashtag",
                    "name": f"#{z}",
                },
            ],
        },
    }
    await post_activity(username, hostname, x["inbox"], body)


async def update_note(username, hostname, x, y):
    t = datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%SZ")
    ts = int(time.time() * 1000)
    body = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": f"{y}#{ts}",
        "type": "Update",
        "actor": f"https://{hostname}/u/{username}",
        "published": t,
        "to": ["https://www.w3.org/ns/activitystreams#Public"],
        "cc": [f"https://{hostname}/u/{username}/followers"],
        "object": {
            "id": y,
            "type": "Note",
            "attributedTo": f"https://{hostname}/u/{username}",
            "content": talk_script("https://localhost"),
            "url": y,
            "updated": t,
            "to": ["https://www.w3.org/ns/activitystreams#Public"],
            "cc": [f"https://{hostname}/u/{username}/followers"],
        },
    }
    await post_activity(username, hostname, x["inbox"], body)


async def delete_tombstone(username, hostname, x, y):
    body = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": f"{y}#Delete",
        "type": "Delete",
        "actor": f"https://{hostname}/u/{username}",
        "object": {
            "id": y,
            "type": "Tombstone",
        },
    }
    await post_activity(username, hostname, x["inbox"], body)


@app.get("/")
def home():
    return PlainTextResponse("StrawberryFields FastAPI")


@app.get("/about")
def about():
    return PlainTextResponse("About: Blank")


@app.get("/u/{username}")
def u_user(username, request: Request):
    hostname = urlparse(CONFIG["origin"]).hostname
    accept_header_field = request.headers.get("accept")
    has_type = False
    if username != CONFIG["actor"][0]["preferredUsername"]:
        raise HTTPException(status_code=404)
    if "application/activity+json" in accept_header_field:
        has_type = True
    if "application/ld+json" in accept_header_field:
        has_type = True
    if "application/json" in accept_header_field:
        has_type = True
    if not has_type:
        body = f"{username}: {CONFIG['actor'][0]['name']}"
        headers = {
            "Cache-Control": f"public, max-age={CONFIG['ttl']}, must-revalidate",
            "Vary": "Accept, Accept-Encoding",
        }
        return PlainTextResponse(body, headers=headers)
    body = {
        "@context": [
            "https://www.w3.org/ns/activitystreams",
            "https://w3id.org/security/v1",
            {
                "schema": "https://schema.org/",
                "PropertyValue": "schema:PropertyValue",
                "value": "schema:value",
                "Key": "sec:Key",
            },
        ],
        "id": f"https://{hostname}/u/{username}",
        "type": "Person",
        "inbox": f"https://{hostname}/u/{username}/inbox",
        "outbox": f"https://{hostname}/u/{username}/outbox",
        "following": f"https://{hostname}/u/{username}/following",
        "followers": f"https://{hostname}/u/{username}/followers",
        "preferredUsername": username,
        "name": CONFIG["actor"][0]["name"],
        "summary": "<p>3.0.0</p>",
        "url": f"https://{hostname}/u/{username}",
        "endpoints": {"sharedInbox": f"https://{hostname}/u/{username}/inbox"},
        "attachment": [
            {
                "type": "PropertyValue",
                "name": "me",
                "value": ME,
            },
        ],
        "icon": {
            "type": "Image",
            "mediaType": "image/png",
            "url": f"https://{hostname}/static/{username}u.png",
        },
        "image": {
            "type": "Image",
            "mediaType": "image/png",
            "url": f"https://{hostname}/static/{username}s.png",
        },
        "publicKey": {
            "id": f"https://{hostname}/u/{username}#Key",
            "type": "Key",
            "owner": f"https://{hostname}/u/{username}",
            "publicKeyPem": public_key_pem,
        },
    }
    headers = {
        "Cache-Control": f"public, max-age={CONFIG['ttl']}, must-revalidate",
        "Vary": "Accept, Accept-Encoding",
        "Content-Type": "application/activity+json",
    }
    return JSONResponse(body, headers=headers)


@app.get("/u/{username}/inbox")
async def inbox(username, request: Request):
    raise HTTPException(status_code=405)


@app.post("/u/{username}/inbox")
async def inbox(username, request: Request):
    hostname = urlparse(CONFIG["origin"]).hostname
    content_type_header_field = request.headers.get("content-type")
    has_type = False
    y = await request.json()
    t = y.get("type") or ""
    aid = y.get("id") or ""
    atype = y.get("type") or ""
    if len(aid) > 1024 or len(atype) > 64:
        raise HTTPException(status_code=400)
    print(f"INBOX {aid} {atype}")
    if username != CONFIG["actor"][0]["preferredUsername"]:
        raise HTTPException(status_code=404)
    if "application/activity+json" in content_type_header_field:
        has_type = True
    if "application/ld+json" in content_type_header_field:
        has_type = True
    if "application/json" in content_type_header_field:
        has_type = True
    if not has_type:
        raise HTTPException(status_code=400)
    if not request.headers.get("digest") or not request.headers.get("signature"):
        raise HTTPException(status_code=400)
    if t == "Accept" or t == "Reject" or t == "Add":
        return Response(status_code=200)
    if t == "Remove" or t == "Like" or t == "Announce":
        return Response(status_code=200)
    if t == "Create" or t == "Update" or t == "Delete":
        return Response(status_code=200)
    if t == "Follow":
        if urlparse(y.get("actor") or "").scheme != "https":
            raise HTTPException(status_code=400)
        x = await get_activity(username, hostname, y.get("actor"))
        if not x:
            raise HTTPException(status_code=500)
        await accept_follow(username, hostname, x, y)
        return Response(status_code=200)
    if t == "Undo":
        z = y.get("object") or {}
        t = z.get("type") or ""
        if t == "Accept" or t == "Like" or t == "Announce":
            return Response(status_code=200)
        if t == "Follow":
            if urlparse(y.get("actor") or "").scheme != "https":
                raise HTTPException(status_code=400)
            x = await get_activity(username, hostname, y.get("actor"))
            if not x:
                raise HTTPException(status_code=500)
            await accept_follow(username, hostname, x, z)
            return Response(status_code=200)
    raise HTTPException(status_code=500)


@app.get("/u/{username}/outbox")
def outbox(username, request: Request):
    hostname = urlparse(CONFIG["origin"]).hostname
    if username != CONFIG["actor"][0]["preferredUsername"]:
        raise HTTPException(status_code=404)
    body = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": f"https://{hostname}/u/{username}/outbox",
        "type": "OrderedCollection",
        "totalItems": 0,
    }
    headers = {"Content-Type": "application/activity+json"}
    return JSONResponse(body, headers=headers)


@app.get("/u/{username}/following")
def following(username, request: Request):
    hostname = urlparse(CONFIG["origin"]).hostname
    if username != CONFIG["actor"][0]["preferredUsername"]:
        raise HTTPException(status_code=404)
    body = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": f"https://{hostname}/u/{username}/following",
        "type": "OrderedCollection",
        "totalItems": 0,
    }
    headers = {"Content-Type": "application/activity+json"}
    return JSONResponse(body, headers=headers)


@app.get("/u/{username}/followers")
def followers(username, request: Request):
    hostname = urlparse(CONFIG["origin"]).hostname
    if username != CONFIG["actor"][0]["preferredUsername"]:
        raise HTTPException(status_code=404)
    body = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": f"https://{hostname}/u/{username}/followers",
        "type": "OrderedCollection",
        "totalItems": 0,
    }
    headers = {"Content-Type": "application/activity+json"}
    return JSONResponse(body, headers=headers)


@app.post("/s/{secret}/u/{username}")
async def s_send(secret, username, request: Request):
    hostname = urlparse(CONFIG["origin"]).hostname
    send = await request.json()
    t = send.get("type") or ""
    if username != CONFIG["actor"][0]["preferredUsername"]:
        raise HTTPException(status_code=404)
    if not secret or secret == "-":
        raise HTTPException(status_code=404)
    if secret != os.getenv("SECRET"):
        raise HTTPException(status_code=404)
    if urlparse(send.get("id") or "").scheme != "https":
        raise HTTPException(status_code=400)
    x = await get_activity(username, hostname, send.get("id"))
    if not x:
        raise HTTPException(status_code=500)
    aid = x.get("id") or ""
    atype = x.get("type") or ""
    if len(aid) > 1024 or len(atype) > 64:
        raise HTTPException(status_code=400)
    if t == "follow":
        await follow(username, hostname, x)
        return Response(status_code=200)
    if t == "undo_follow":
        await undo_follow(username, hostname, x)
        return Response(status_code=200)
    if t == "like":
        if urlparse(x.get("attributedTo") or "").scheme != "https":
            raise HTTPException(status_code=400)
        y = await get_activity(username, hostname, x.get("attributedTo"))
        if not y:
            raise HTTPException(status_code=500)
        await like(username, hostname, x, y)
        return Response(status_code=200)
    if t == "undo_like":
        if urlparse(x.get("attributedTo") or "").scheme != "https":
            raise HTTPException(status_code=400)
        y = await get_activity(username, hostname, x.get("attributedTo"))
        if not y:
            raise HTTPException(status_code=500)
        await undo_like(username, hostname, x, y)
        return Response(status_code=200)
    if t == "announce":
        if urlparse(x.get("attributedTo") or "").scheme != "https":
            raise HTTPException(status_code=400)
        y = await get_activity(username, hostname, x.get("attributedTo"))
        if not y:
            raise HTTPException(status_code=500)
        await announce(username, hostname, x, y)
        return Response(status_code=200)
    if t == "undo_announce":
        if urlparse(x.get("attributedTo") or "").scheme != "https":
            raise HTTPException(status_code=400)
        y = await get_activity(username, hostname, x.get("attributedTo"))
        if not y:
            raise HTTPException(status_code=500)
        z = send.get("url") or f"https://{hostname}/u/{username}/s/00000000000000000000000000000000"
        if urlparse(z).scheme != "https":
            raise HTTPException(status_code=400)
        await undo_announce(username, hostname, x, y, z)
        return Response(status_code=200)
    if t == "create_note":
        y = send.get("url") or "https://localhost"
        if urlparse(y).scheme != "https":
            raise HTTPException(status_code=400)
        await create_note(username, hostname, x, y)
        return Response(status_code=200)
    if t == "create_note_image":
        y = send.get("url") or f"https://{hostname}/static/logo.png"
        if urlparse(y).scheme != "https" or urlparse(y).hostname != hostname:
            raise HTTPException(status_code=400)
        z = "image/png"
        if y.endswith(".jpg") or y.endswith(".jpeg"):
            z = "image/jpeg"
        if y.endswith(".svg"):
            z = "image/svg+xml"
        if y.endswith(".gif"):
            z = "image/gif"
        if y.endswith(".webp"):
            z = "image/webp"
        if y.endswith(".avif"):
            z = "image/avif"
        await create_note_image(username, hostname, x, y, z)
        return Response(status_code=200)
    if t == "create_note_mention":
        if urlparse(x.get("attributedTo") or "").scheme != "https":
            raise HTTPException(status_code=400)
        y = await get_activity(username, hostname, x.get("attributedTo"))
        if not y:
            raise HTTPException(status_code=500)
        z = send.get("url") or "https://localhost"
        if urlparse(z).scheme != "https":
            raise HTTPException(status_code=400)
        await create_note_mention(username, hostname, x, y, z)
        return Response(status_code=200)
    if t == "create_note_hashtag":
        y = send.get("url") or "https://localhost"
        if urlparse(y).scheme != "https":
            raise HTTPException(status_code=400)
        z = send.get("tag") or "Hashtag"
        await create_note_hashtag(username, hostname, x, y, z)
        return Response(status_code=200)
    if t == "update_note":
        y = send.get("url") or f"https://{hostname}/u/{username}/s/00000000000000000000000000000000"
        if urlparse(y).scheme != "https":
            raise HTTPException(status_code=400)
        await update_note(username, hostname, x, y)
        return Response(status_code=200)
    if t == "delete_tombstone":
        y = send.get("url") or f"https://{hostname}/u/{username}/s/00000000000000000000000000000000"
        if urlparse(y).scheme != "https":
            raise HTTPException(status_code=400)
        await delete_tombstone(username, hostname, x, y)
        return Response(status_code=200)
    print(f"TYPE {aid} {atype}")
    return Response(status_code=200)


@app.get("/.well-known/nodeinfo")
def nodeinfo(request: Request):
    hostname = urlparse(CONFIG["origin"]).hostname
    body = {
        "links": [
            {
                "rel": "http://nodeinfo.diaspora.software/ns/schema/2.0",
                "href": f"https://{hostname}/nodeinfo/2.0.json",
            },
            {
                "rel": "http://nodeinfo.diaspora.software/ns/schema/2.1",
                "href": f"https://{hostname}/nodeinfo/2.1.json",
            },
        ],
    }
    headers = {
        "Cache-Control": f"public, max-age={CONFIG['ttl']}, must-revalidate",
        "Vary": "Accept, Accept-Encoding",
    }
    return JSONResponse(body, headers=headers)


@app.get("/.well-known/webfinger")
def webfinger(request: Request):
    username = CONFIG["actor"][0]["preferredUsername"]
    hostname = urlparse(CONFIG["origin"]).hostname
    p443 = f"https://{hostname}:443/"
    resource = request.query_params.get("resource")
    has_resource = False
    if resource.startswith(p443):
        resource = f"https://{hostname}/{resource[len(p443):]}"
    if resource == f"acct:{username}@{hostname}":
        has_resource = True
    if resource == f"mailto:{username}@{hostname}":
        has_resource = True
    if resource == f"https://{hostname}/@{username}":
        has_resource = True
    if resource == f"https://{hostname}/u/{username}":
        has_resource = True
    if resource == f"https://{hostname}/user/{username}":
        has_resource = True
    if resource == f"https://{hostname}/users/{username}":
        has_resource = True
    if not has_resource:
        raise HTTPException(status_code=404)
    body = {
        "subject": f"acct:{username}@{hostname}",
        "aliases": [
            f"mailto:{username}@{hostname}",
            f"https://{hostname}/@{username}",
            f"https://{hostname}/u/{username}",
            f"https://{hostname}/user/{username}",
            f"https://{hostname}/users/{username}",
        ],
        "links": [
            {
                "rel": "self",
                "type": "application/activity+json",
                "href": f"https://{hostname}/u/{username}",
            },
            {
                "rel": "http://webfinger.net/rel/avatar",
                "type": "image/png",
                "href": f"https://{hostname}/static/{username}u.png",
            },
            {
                "rel": "http://webfinger.net/rel/profile-page",
                "type": "text/plain",
                "href": f"https://{hostname}/u/{username}",
            },
        ],
    }
    headers = {
        "Cache-Control": f"public, max-age={CONFIG['ttl']}, must-revalidate",
        "Vary": "Accept, Accept-Encoding",
        "Content-Type": "application/jrd+json",
    }
    return JSONResponse(body, headers=headers)


@app.get("/@")
@app.get("/u")
@app.get("/user")
@app.get("/users")
def u():
    return RedirectResponse("/", status_code=302)


@app.get("/users/{username}")
@app.get("/user/{username}")
@app.get("/@{username}")
def uu(username):
    return RedirectResponse(f"/u/{username}", status_code=302)


app.mount("/", StaticFiles(directory="static"), name="static")
